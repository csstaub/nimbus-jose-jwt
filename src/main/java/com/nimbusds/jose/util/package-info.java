/**
 * Base64, Base64URL, compression and JSON utility classes.
 */
package com.nimbusds.jose.util;
